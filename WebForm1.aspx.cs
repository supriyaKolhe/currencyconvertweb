﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CurrencyConvertWeb
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void TextBox3_TextChanged(object sender, EventArgs e)
        {
            double textbox = Convert.ToDouble(TextBox3.Text);
            int i, j;
            double sum = 0;

            double[,] rates = { { 1, 113.84, 74.45 }, { 113.84, 1, 0.65 }, { 74.45, 0.65, 1 } };

            double d1 = Convert.ToDouble(DropDownList1.SelectedIndex);
            double d2 = Convert.ToDouble(DropDownList2.SelectedIndex);

            for (i = 0; i < 3; i++)//row
            {
                for (j = 0; j < 3; j++)//column
                {
                    if(d1==i && d2 == j)
                    {
                        sum = rates[i, j];
                        sum *= textbox;
                    }
                }
            }
            TextBox2.Text = Math.Round(sum).ToString();
        }
    }
}